package com.example.rxjava_mvvm_repopattern_demo.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "repo")
public class Repo {

    @PrimaryKey
    @NonNull
    private String id;

    @SerializedName(value = "name")
    @ColumnInfo(name = "name")
    private String repoName;
    private String description;
    private String language;
    @ColumnInfo(name = "starCount")
    @SerializedName(value = "stargazers_count")
    private int starCount;

    public Repo(String repoName,String description,String language,int starCount){
        this.description = description;
        this.language = language;
        this.repoName = repoName;
        this.starCount =starCount;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getStarCount() {
        return starCount;
    }

    public void setStarCount(int starCount) {
        this.starCount = starCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
