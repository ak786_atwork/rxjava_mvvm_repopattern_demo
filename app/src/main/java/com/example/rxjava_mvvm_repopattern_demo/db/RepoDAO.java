package com.example.rxjava_mvvm_repopattern_demo.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RepoDAO {

    @Query("SELECT * FROM repo ")
    List<Repo> fetchMyRepos();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveMyRepos(List<Repo> repos);
}
