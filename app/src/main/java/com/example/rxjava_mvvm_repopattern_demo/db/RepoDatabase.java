package com.example.rxjava_mvvm_repopattern_demo.db;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Repo.class},version = 1)
public abstract class RepoDatabase extends RoomDatabase {

    public abstract RepoDAO getRepoDAO();

    static RepoDatabase database;

    public static RepoDatabase getInstance(Context context){
        if (database == null){
            database = Room.databaseBuilder(context,RepoDatabase.class,"repo.db").build();
        }
        return database;
    }
}
