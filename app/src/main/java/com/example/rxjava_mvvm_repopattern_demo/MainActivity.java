package com.example.rxjava_mvvm_repopattern_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MAIN_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Observable<Integer> dataStream = createBasicStream();
//        Observer<Integer> observer = createObserver();
//        dataStream.subscribe(observer);

        //passing Consumer which is a functional interface
     /*   createFromArray().subscribe((it) -> {
            showLog(it.toString());
        });*/

//        createFromIterable().subscribe((data) -> showLog(data.toString()));
//        createFromRange().subscribe((data) -> showLog(data.toString()));
//        createFromRangeWithRepeat().subscribe((data) -> showLog(data.toString()));
//        createFromInterval().subscribe((data) -> showLog(data.toString()));
//        createFromTimer().subscribe((data) -> showLog(data.toString()));
//        createFromFilter().subscribe((data) -> showLog(data.toString()));
//        createTakeLast().subscribe((data) -> showLog(data.toString()));
//        createTake().subscribe((data) -> showLog(data.toString()));
      /*  createTimeOut("ak").subscribe(
                (data) -> showLog(data),

                (e) -> showLog(" error"+e.toString())
        );*/

//        createDistinct().subscribe((data) -> showLog(data.toString()));
//        createStartWith().subscribe((data) -> showLog(data.toString()));
//        createMerge().subscribe((data) -> showLog(data.toString()));
//        createConcat().subscribe((data) -> showLog(data.toString()));
//        createZip().subscribe((data) -> showLog(data.toString()));
//        createMap().subscribe((data) -> showLog(data.toString()));
//        createFlatMap().subscribe((data) -> showLog(data.toString()));


        //Subject
        PublishSubject<String> professor = PublishSubject.create();











/*        createFromArray().subscribe(new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Integer integer) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });*/

    }

    private Observable<Integer> createTake() {
        return Observable.just(1,2,3,4).take(2);
    }

    private void showLog(String info) {
        Log.d(TAG, "showLog: "+info);
    }

    private Observable<Integer> createBasicStream() {
        return Observable.just(10,20,30,40);
    }

    private Observable<Integer> createFromArray() {
        return Observable.fromArray(10,20,30,40);
    }

    private Observable<Integer> createFromRange(){
        return Observable.range(20,10);
    }

    private Observable<Long> createFromInterval(){
        return Observable.interval(1, TimeUnit.SECONDS);
    }

    private Observable<Long> createFromTimer(){
        return Observable.timer(1,TimeUnit.SECONDS);
    }

    private Observable<Long> createFromIntervalWithCondition(){
        return Observable.interval(1, TimeUnit.SECONDS).takeWhile((i) -> i<10);
    }

    private Observable<Integer> createFromFilter(){
        return Observable.just(1,2,3,4).filter(i -> i<3 );
    }

    private Observable<Integer> createStartWith(){
        return Observable.just(1,2,3,4).startWith(Observable.just(5,6));
    }

    private Observable<Integer> createMerge(){
        Observable<Integer> dataStream1 = Observable.just(1,2,3);
        Observable<Integer> dataStream2 = Observable.just(0,4,5);
//        return Observable.merge(dataStream1,dataStream2);
        return dataStream1.mergeWith(dataStream2);
    }

    private Observable<Integer> createConcat(){
        Observable<Integer> dataStream1 = Observable.just(1,2,3);
        Observable<Integer> dataStream2 = Observable.just(0,4,5);
        return dataStream1.concatWith(dataStream2);
    }


    private Observable<Integer> createDistinct(){
        return Observable.just(1,2,3,4,1,2).distinct();
    }



    private Observable<Integer> createTakeLast(){
        return Observable.just(1,2,3,4).takeLast(2);
    }


    private Observable<Integer> createFromRangeWithRepeat(){
        return Observable.range(20,10).repeat(2);
    }

    private Observable<String> createTimeOut(String name){
        return Observable.fromCallable(
                () -> {
                    if (name.equals("ak1")) {
                        Thread.sleep(150);
                    }
                    return name;
                }
        ).timeout(100,TimeUnit.MILLISECONDS);

    }



    private Observable<Integer> createFromIterable() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(30);
        return Observable.fromIterable(arrayList);
    }

    private Observer<Integer> createObserver() {
        return new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {
                showLog("subscribed");
            }

            @Override
            public void onNext(Integer integer) {
                showLog("new data is received : "+integer);
            }

            @Override
            public void onError(Throwable e) {
                showLog("error : "+e.toString());
            }

            @Override
            public void onComplete() {
                showLog("stream completed");
            }
        };
    }

    //combining operator
    private Observable<Integer> createZip(){
        Observable<Integer> dataStream1 = Observable.just(1,2,3);
        Observable<Integer> dataStream2 = Observable.just(0,4,5);
        return dataStream1.zipWith(dataStream2,(x,y) -> x*y);
    }

    //map transform the items by an observable by applying a function to each item
    private Observable<Integer> createMap(){
        Observable<Integer> dataStream1 = Observable.just(1,2,3);
        return dataStream1.map((x) -> x*10);
    }

    private Observable<String> createFlatMap() {
        Observable<Integer> dataStream1 = Observable.just(1,2,3);
        return dataStream1.flatMap((i) -> getRandomName(i));
    }

    private Observable<String> getRandomName(int i){
        String[] names = {"ak","sk","nk"};
        return Observable.just(names[i%names.length]);
    }
}
