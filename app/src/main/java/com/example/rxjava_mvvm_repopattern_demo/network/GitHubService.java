package com.example.rxjava_mvvm_repopattern_demo.network;

import com.example.rxjava_mvvm_repopattern_demo.db.Repo;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubService {

    @GET("users/{user}/starred")
    Observable<List<Repo>> getStarredRepos(@Path("user") String username);
}
