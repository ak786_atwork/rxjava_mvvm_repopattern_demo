package com.example.rxjava_mvvm_repopattern_demo.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Singleton class
public class GithubApiClient {

    private static HttpLoggingInterceptor httpLoggingInterceptor;
    private static String BASE_URL = "https://api.github.com";
    private static Retrofit retrofit;
    private static GitHubService gitHubService;

    private static void init(){
        httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60,TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        gitHubService = retrofit.create(GitHubService.class);

    }

    public static GitHubService getGithubService() {
        if (gitHubService == null)
            init();
        return gitHubService;
    }

}
