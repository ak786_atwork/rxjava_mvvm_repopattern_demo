package com.example.rxjava_mvvm_repopattern_demo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.rxjava_mvvm_repopattern_demo.adapter.GitHubRepoAdapter;
import com.example.rxjava_mvvm_repopattern_demo.db.Repo;
import com.example.rxjava_mvvm_repopattern_demo.viewmodel.RepoViewModel;

import java.util.ArrayList;
import java.util.List;

public class MyStarReposActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Repo> repoArrayList;
    GitHubRepoAdapter gitHubRepoAdapter;
    private RepoViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_star_repos);

        recyclerView = findViewById(R.id.recycler_view);
        repoArrayList = new ArrayList<>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);

        gitHubRepoAdapter = new GitHubRepoAdapter();
        gitHubRepoAdapter.setReposList(repoArrayList);

        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(gitHubRepoAdapter);

        viewModel = ViewModelProviders.of(this).get(RepoViewModel.class);

        getStarredRepos(viewModel);
        observeMyStars(viewModel);
    }

    private void observeMyStars(RepoViewModel viewModel) {
        viewModel.getRepoLiveData().observe(this, (repos -> gitHubRepoAdapter.setReposList(repos)));
    }

    private void getStarredRepos(RepoViewModel viewModel){
        viewModel.getMyStarredList("mrabelwahed");
    }


}
