package com.example.rxjava_mvvm_repopattern_demo;

import android.app.Application;
import android.content.Context;

public class RxApp extends Application {

    public static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

}
