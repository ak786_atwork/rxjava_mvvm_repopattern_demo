package com.example.rxjava_mvvm_repopattern_demo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rxjava_mvvm_repopattern_demo.R;
import com.example.rxjava_mvvm_repopattern_demo.db.Repo;

import java.util.ArrayList;
import java.util.List;

public class GitHubRepoAdapter extends RecyclerView.Adapter<GitHubRepoAdapter.StarRepoViewHolder> {

    private List<Repo> repos = new ArrayList<>();

    @NonNull
    @Override
    public StarRepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.stars_item,null);
        return new StarRepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StarRepoViewHolder holder, int position) {
        holder.repoName.setText(repos.get(position).getRepoName());
        holder.repoStars.setText(String.valueOf(repos.get(position).getStarCount())+" stars");

        if (repos.get(position).getDescription() == null || repos.get(position).getDescription().length()==0)
            holder.repoDescription.setText("No Description");
        else
            holder.repoDescription.setText(repos.get(position).getDescription());

        if (repos.get(position).getLanguage() == null || repos.get(position).getLanguage().length()==0)
            holder.repoDescription.setText("No Language");
        else
            holder.repoLanguage.setText(repos.get(position).getLanguage());


    }

    @Override
    public int getItemCount() {
        return repos.size();
    }

    public void setReposList(List<Repo> repos) {
        this.repos = repos;
        notifyDataSetChanged();
    }

    class StarRepoViewHolder extends RecyclerView.ViewHolder{

        public TextView repoName;
        public TextView repoDescription;
        public TextView repoLanguage;
        public TextView repoStars;

        public StarRepoViewHolder(@NonNull View itemView) {
            super(itemView);
            repoName = itemView.findViewById(R.id.repoName);
            repoDescription = itemView.findViewById(R.id.description);
            repoLanguage = itemView.findViewById(R.id.language);
            repoStars = itemView.findViewById(R.id.starsCount);
        }
    }
}
