package com.example.rxjava_mvvm_repopattern_demo.repository;

import com.example.rxjava_mvvm_repopattern_demo.RxApp;
import com.example.rxjava_mvvm_repopattern_demo.db.Repo;
import com.example.rxjava_mvvm_repopattern_demo.db.RepoDatabase;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class RepoLocalSource implements RepoDataSourceInterface {
    @Override
    public Observable<List<Repo>> fetchRepos(String username) {
        return Observable.fromCallable(() -> RepoDatabase.getInstance(RxApp.context).getRepoDAO().fetchMyRepos());
    }

    @Override
    public void saveRepos(List<Repo> repos) {
        RepoDatabase.getInstance(RxApp.context).getRepoDAO().saveMyRepos(repos);
    }
}
