package com.example.rxjava_mvvm_repopattern_demo.repository;

import com.example.rxjava_mvvm_repopattern_demo.db.Repo;
import java.util.List;

import io.reactivex.Observable;

public class RepoRepository implements RepoDataSourceInterface {

    private RepoRemoteSource repoRemoteSource;
    private RepoLocalSource repoLocalSource;
    public RepoRepository(RepoLocalSource repoLocalSource, RepoRemoteSource repoRemoteSource)  {
        this.repoLocalSource =repoLocalSource;
        this.repoRemoteSource = repoRemoteSource;
    }


    @Override
    public Observable<List<Repo>> fetchRepos(String username) {
        return Observable.concatArray(repoLocalSource.fetchRepos(username),repoRemoteSource.fetchRepos(username))
                .doOnNext((repos -> saveRepos(repos)))
                .onErrorResumeNext(Observable.empty());
    }

    @Override
    public void saveRepos(List<Repo> repos) {
        repoLocalSource.saveRepos(repos);
    }
}
