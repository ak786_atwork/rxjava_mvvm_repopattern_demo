package com.example.rxjava_mvvm_repopattern_demo.repository;

import com.example.rxjava_mvvm_repopattern_demo.db.Repo;

import java.util.List;

import io.reactivex.Observable;

public interface RepoDataSourceInterface {
    Observable<List<Repo>> fetchRepos(String username);
    void saveRepos(List<Repo> repos);
}
