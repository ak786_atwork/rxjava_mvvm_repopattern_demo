package com.example.rxjava_mvvm_repopattern_demo.repository;

import com.example.rxjava_mvvm_repopattern_demo.db.Repo;
import com.example.rxjava_mvvm_repopattern_demo.network.GithubApiClient;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class RepoRemoteSource implements RepoDataSourceInterface {
    @Override
    public Observable<List<Repo>> fetchRepos(String username) {
        return GithubApiClient.getGithubService().getStarredRepos(username);
    }

    @Override
    public void saveRepos(List<Repo> repos) {

    }
}
