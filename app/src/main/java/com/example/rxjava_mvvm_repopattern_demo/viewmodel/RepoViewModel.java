package com.example.rxjava_mvvm_repopattern_demo.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.rxjava_mvvm_repopattern_demo.db.Repo;
import com.example.rxjava_mvvm_repopattern_demo.repository.RepoLocalSource;
import com.example.rxjava_mvvm_repopattern_demo.repository.RepoRemoteSource;
import com.example.rxjava_mvvm_repopattern_demo.repository.RepoRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RepoViewModel extends ViewModel {

    //to avoid memory leak issue
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<List<Repo>> repoLiveData = new MutableLiveData<>();

    private RepoRepository repoRepository = new RepoRepository(new RepoLocalSource(),new RepoRemoteSource());

    public void getMyStarredList(String username) {
         Disposable repoDisposable = repoRepository.fetchRepos(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (data) -> repoLiveData.postValue(data)
                );
         compositeDisposable.add(repoDisposable);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
    }

    public MutableLiveData<List<Repo>> getRepoLiveData() {
        return repoLiveData;
    }

}
